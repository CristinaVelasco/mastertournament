/**
 * @author Cristina Velasco 
 * @date  07/04/2017
 * @description Class that mocks the response of the webServiceGetRegistrations when the
 *				response is the expected.
 */
@isTest
global class API_CalloutOK implements HttpCalloutMock{
	static final String BODY = '[{"id":1,"match":"123","type":"Annotation","minute":20,"points":1,"player":"Ext_1111","description":""},{"id":2,"match":"123","type":"Annotation","minute":36,"points":1,"player":"Ext_1111","description":""},{"id":3,"match":"123","type":"Annotation","minute":56,"points":1,"player":"Ext_1111","description":""},{"id":4,"match":"123","type":"Foul","minute":24,"points":null,"player":"Ext_1111","description":"Yellow card"}]';
	static final Integer RETURN_CODE = 200;
    
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setBody(BODY);
        res.setStatusCode(RETURN_CODE);
        res.setHeader('Content-Type', 'application/json');
        return res;
    }
}