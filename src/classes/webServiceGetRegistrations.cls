/*
    ~ Author            : Cristina Velasco
    ~ Created Date      : 07/04/2017 (dd/mm/yyyy) 
    ~ Description       : An exposed custom REST API for Salesforce, the registrations will be updated  
                          

*/
global class webServiceGetRegistrations {

    @RemoteAction 
    webservice static String getAllRegistrations(){
    	
        Http h = new Http();
		HttpRequest req = new HttpRequest();
        List<Match_registration__c> listMatchReg = new List<Match_registration__c>();
        
        Registration_API__c customSetting = Registration_API__c.getValues('WebService');
        String authEndpointURL = customSetting.Authentication_Endpoint__c;
        
        try{
			req.setEndpoint(authEndpointURL);
			req.setMethod('GET');
			req.setHeader('content-type', 'application/json');
			req.setTimeout(60000);
			
			HttpResponse res = h.send(req);
			system.debug('##### res: '+res.getBody());
			
			if(res.getStatusCode() == 200) {
                List<JsonClassParse> responseList = (List<JsonClassParse>)JSON.deserialize(res.getBody(), List<JsonClassParse>.class);
                system.debug('##### responseList: '+responseList);
                if (responseList != null){
                    
                    for (JsonClassParse i : responseList){
                        String playerId = getPlayerId(i.player);
                        String teamId = getTeamId(playerId);
                        String matchId = getMatchId(i.match);
                        String recordTypeId = getRecordTypeId(i.type);
                        
                        Match_registration__c matchReg = new Match_registration__c(
                            Match_Registration_Relationship__c = matchId != null ? matchId : ''  ,
                            Minute__c = i.minute,
                            Player_Registration__c = playerId != null ? playerId : ''  ,
                            Points__c = i.points,
                            Description__c = i.description,
                            Team_Registration__c = teamId != null ? teamId : '',
							RecordTypeId = recordTypeId,
                            ExtId__c = i.id
                    	);
                        system.debug('##### matchReg: '+matchReg);
                    	listMatchReg.add(matchReg);
                    }
                    system.debug('##### listMatchReg: '+listMatchReg);
                    upsert listMatchReg ExtId__c;                 
                }    
                return 'Success';
            } else {
                return 'Failed to get the data';
            }
        } catch(Exception ex){
        	return 'Failed to get the data';
        }                
        
    }
    
    public static String getTeamId (String idPlayer){
        Account team = [SELECT ParentId FROM Account where id =: idPlayer][0];
        system.debug('##### string teamId: '+String.valueOf(team.ParentId));
        return String.valueOf(team.ParentId);
    }
    
    public static String getMatchId (String idMatch){
        Match__c match = [SELECT Id FROM Match__c where External_Ref__c =: idMatch][0];
		system.debug('##### string match: '+String.valueOf(match.Id));
        return String.valueOf(match.Id);
    }
    
	public static String getPlayerId (String idPlayer){
        Account player = [SELECT Id FROM Account where External_Account_Id__c =: idPlayer][0];
        system.debug('##### string player: '+String.valueOf(player.Id));
        return String.valueOf(player.Id);
    }
    
    public static String getRecordTypeId (String recordTypeName){
        RecordType recordType = [SELECT Id FROM RecordType where Name =: recordTypeName][0];
        system.debug('##### string recordType: '+String.valueOf(recordType.Id));
        return String.valueOf(recordType.Id);
    }
}