public class JsonClassParse {
	public String id{get;Set;}
	public String match{get;Set;}
	public String type{get;Set;}
	public Integer minute{get;Set;}
	public Integer points{get;Set;}
	public String player{get;Set;}
	public String description{get;Set;}
}