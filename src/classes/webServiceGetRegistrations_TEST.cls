/**
 * @author Cristina Velasco 
 * @date 07/04/2017
 * @description Class for testing the behavior of webServiceGetRegistrations.
 */
@isTest
public class webServiceGetRegistrations_TEST {
    
    static void createData(){

        Registration_API__c customSetting = new Registration_API__c();
        customSetting.Name = 'WebService';
        customSetting.Authentication_Endpoint__c = 'http://fakeapi.casorranbobed.com/v1/registration';
        insert customSetting;
        
        Tournament__c tournament = new Tournament__c();
	    tournament.Sport__c = 'Football';
        tournament.Name = 'Tournament Test';
        tournament.Start_Date__c = Date.today();
        tournament.End_Date__c =  Date.today();
        tournament.Organization_System__c = 'Elimination System';
        insert tournament;
        
        Account visitorTeamAccount = new Account();
        visitorTeamAccount.Name = 'Team Visitor Test';
        visitorTeamAccount.Sport_team__c = 'Football';
        visitorTeamAccount.RecordTypeId = '0120Y0000005s6sQAA';
        insert visitorTeamAccount;
        
        Account localTeamAccount = new Account();
        localTeamAccount.Name = 'Team Local Test';
        localTeamAccount.Sport_team__c = 'Football';
        localTeamAccount.RecordTypeId = '0120Y0000005s6sQAA';
        insert localTeamAccount;
       
        Account playerAccount = new Account();
        playerAccount.Name = 'Player Test';
        playerAccount.ParentId = localTeamAccount.Id;
        playerAccount.RecordTypeId = '0120Y0000005s6xQAA';
        playerAccount.External_Account_Id__c = 'Ext_1111';
        insert playerAccount;
                
        Match__c match = new Match__c(); 
        match.External_Ref__c = '123';
        match.Local_Team__c = localTeamAccount.Id;
        match.Visitor_Team__c = visitorTeamAccount.Id;
        match.Match_date_time__c = Datetime.now();
        match.TournamentRelation__c = tournament.Id;
		match.Name = 'Test Name Match';
		insert match;
        
    }
    
    static testmethod void getTest(){ 
        createData();
        test.startTest();
		Test.setMock(HttpCalloutMock.class, new API_CalloutOK());
        
        String response = webServiceGetRegistrations.getAllRegistrations();
        
        Boolean assertCondition = response.contains('Success');
        
        System.assertEquals(true, assertCondition); 
		test.stopTest();
    }
    
    

}